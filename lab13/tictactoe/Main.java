package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();
            int row = 0; //temporary local variable you have to assign a value!!
            int col = 0;

            boolean invalidRow = false;
            boolean invalidCol = false;
            do {
                System.out.print("Player " + player + " enter row number:");
                try {
                     row = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid integer!!");
                    invalidRow = false;
                } catch (NumberFormatException ex) {
                    System.out.println("Invalid integer!!");
                    invalidRow = true;
                }
            }while (invalidRow);

            do {
                System.out.print("Player " + player + " enter col number:");
                try {
                    col = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid integer!!");
                    invalidCol = false;
                } catch (NumberFormatException ex) {
                    System.out.println("Invalid integer!!");
                    invalidCol = true;
                }
            }while (invalidCol);

            /*System.out.print("Player " + player + " enter column number:");
            int col = Integer.valueOf(reader.nextLine());
*/
            try {
                board.move(row, col);
            } catch (InvalidMoveException e) {
                throw new RuntimeException(e.getMessage());
            }
            System.out.println(board);
        }


        reader.close();
    }


}
