public class MyDateTime {
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString(){
        return date + " " + time;
    }
    public void incrementDay(){
        date.incrementDay();
    }

    public void incrementHour(){
        incrementHour(1);
    }
    public void incrementHour(int diff){
        int dayDiff = time.incrementHour(diff);

        if(dayDiff<0){
            date.decrementDay(-dayDiff);
        }
        else
            date.incrementDay(dayDiff);
    }


    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {
        int dayDiff = time.incrementMinute(diff);

        if(dayDiff<0){
            date.decrementDay(-dayDiff);
        }
        else
            date.incrementDay(dayDiff);

    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int diff) {
        date.incrementDay(diff);
    }
    public void decrementMonth(int diff){
        date.decrementMonth(diff);
    }
    public void decrementDay(int diff) {
        date.decrementDay(diff);
    }
    public void incrementMonth(){
        date.incrementMonth();
    }

    public void incrementMonth(int diff){
        date.incrementMonth(diff);
    }
    public void decrementYear(int diff){
        date.decrementYear(diff);
    }
    public void incrementYear(){
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime MyDateTime2){
        if(date.isBefore(MyDateTime2.date)) {return true;}
        else if(date.isAfter(MyDateTime2.date)){return false;}
        else{
            int a = Integer.parseInt(time.toString().replaceAll(":",""));
            int b = Integer.parseInt(MyDateTime2.time.toString().replaceAll(":",""));

            return a<b;
        }

    }
    public boolean isAfter(MyDateTime MyDateTime2){
        if(date.isAfter(MyDateTime2.date)) {return true;}
        else if(date.isBefore(MyDateTime2.date)){return false;}
        else{
            int a = Integer.parseInt(time.toString().replaceAll(":",""));
            int b = Integer.parseInt(MyDateTime2.time.toString().replaceAll(":",""));

            return b<a;
        }

    }
    public String dayTimeDifference(MyDateTime MyDateTime2){
        int hourDiff;
        int minuteDiff;
        int dayDiff;

        dayDiff = date.dayDifference(MyDateTime2.date);
        hourDiff =Math.abs(time.getHour()-MyDateTime2.time.getHour()) ;
        minuteDiff = Math.abs(time.getMinute()-MyDateTime2.time.getMinute());

        return (dayDiff < 1 ?  "" : dayDiff + " day(s) ") + (hourDiff<1 ? "": hourDiff + " hour(s) ") + (minuteDiff<1 ? "" : minuteDiff +" minute(s) ");





    }
}
