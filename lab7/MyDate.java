public class MyDate {
    int day;
    int month;
    int year;

    int[] maxDays ={31,29,31,30,31,30,31,31,30,31,30,31};


    public MyDate(int day, int month, int year){
        this.day=day;
        this.month=month;
        this.year=year;
    }
    public boolean leapYear(){
        return year % 4 == 0;

    }
    public String toString(){
       return year + "-" + (month<10 ? ("0"+ month):month) + "-"+ (day<10 ? ("0"+ day):day);

    }

    public void incrementDay(){
        day++;
        if (day > maxDays[month-1]){
            day = 1;
            incrementMonth();
        }else if(month == 2 && day == 29 && !leapYear()){
            day = 1;
            incrementMonth();
        }

    }
    public void incrementYear(int diff){
        year += diff;
        if(month == 2 && day == 29 && !leapYear()){
            day = 28;
        }
    }

    public void decrementDay(){
        day--;
        if (day == 0){

            decrementMonth();
            day=maxDays[month-1];
            if(!leapYear() && month == 2){
                day = 28;
            }else {
                day = maxDays[month - 1];
            }
        }
    }

    public void decrementYear(){
        incrementYear(-1);
    }
    public void decrementMonth(){
        month--;
        if(month == 2 && !leapYear() && day == maxDays[month]){
            day = 28;
        }else if(day == maxDays[month]){
            day = maxDays[month-1];
        }else if(leapYear() && month == 2 && day == maxDays[month]) {
            day = 29;
        }

    }

    public void incrementDay(int diff){
        while ( diff > 0){
            incrementDay();
            diff--;

        }

    }

    public void decrementMonth(int diff){
        incrementMonth(-diff);
    }

    public void incrementMonth(int diff) {
        month = month + diff;

        int yearDiff = (month-1)/12;
        int newMonth = ((month -1) % 12) +1 ;

        if (newMonth < 0)
            yearDiff --;
        year+=yearDiff;



        month = newMonth < 0 ? newMonth+12:newMonth;

        if (day > maxDays[month-1]){
            day = maxDays[month-1];
        }else if(month == 2 && day == 29 && !leapYear()){
            day = 28;
        }


    }

    public void decrementDay(int diff){
        while (diff >0){
            decrementDay();
            diff--;
        }

    }
    public void decrementYear(int diff){
        incrementYear(-diff);
    }
    public void incrementMonth(){
        incrementMonth(1);
    }
    public void incrementYear(){
        incrementYear(1);
    }

    public boolean isBefore(MyDate date2){
        //too complicated make it simple!
        if(this.year > date2.year ){return false;}
        else if(this.year<date2.year){return true;}
        else if(this.year == date2.year){
            if(this.month>date2.month){return false;}
            else if(this.month<date2.month){return true;}
            else if(this.month==date2.month){
                if(this.day>date2.day){return false;}
                else if(this.day<date2.day){return true;}
                else return false;
            }

        }
        return false;
    }

    public boolean isAfter(MyDate date2){
        if (!isBefore(date2)){return true;}
        else return false;
    }

    public int dayDifference(MyDate date2){
        //there is leap year problem.When you select 02-28, day difference's result 1 plus than the correct result
         boolean after = isAfter(date2);
         int monthDiff=0;
         int dayDiff=0;
         if(this.year == date2.year){
             if(after){
                 if(this.month== date2.month){dayDiff=this.day- date2.day;}
                 else{
                     monthDiff = this.month- date2.month;
                     for(int i = 1;i<monthDiff;i++){
                         dayDiff=maxDays[(((date2.month+i)-1)%12)+1];
                     }
                     dayDiff = dayDiff + this.day+((maxDays[date2.month-1]-date2.day));
                 }
             }
             else{
                 if(this.month== date2.month){dayDiff=-(this.day- date2.day);}
                 else{
                     monthDiff = -(this.month- date2.month);
                     for(int i = 1;i<monthDiff;i++){
                         dayDiff=maxDays[(((this.month+i)-1)%12)+1];
                     }
                     dayDiff = dayDiff + date2.day+(maxDays[this.month-1]-this.day);
                 }
             }
         }
         else if (this.year> date2.year){
             monthDiff = this.month +(12- date2.month)+(12*((this.year - date2.year)-1));
             for(int i=1;i<monthDiff;i++){
                 dayDiff = maxDays[(((date2.month+i)-1)%12)+1];
             }
             dayDiff = dayDiff+this.day+(maxDays[date2.month-1]- date2.day);
         }
         else if(this.year< date2.year){
             monthDiff = date2.month +(12- this.month)+(12*((date2.year-this.year)-1));
             for(int i=1;i<monthDiff;i++){
                 dayDiff = maxDays[(((this.month+i)-1)%12)+1];
             }
             dayDiff = dayDiff+ date2.day+(maxDays[this.month-1]-this.day);

         }

     return dayDiff;
    }

}
